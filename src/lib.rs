//! DSP Algorithms
//!
//! Zero Initial Conditions: We assume all inputs prior to the
//! zero index are zero.

/// Calculate the frequency of a sinusoid.
///
/// In discrete time the number of samples has no physical
/// dimension (just a counter). The peridocity is measured
/// by how many samples there are before a pattern repeats.
///
/// * `time_sec`: The time in seconds between samples
/// * `samp_sec`: The number of samples per second.
/// * `m`: number of samples
///
/// Note that the notation of each is ususlly:
///
/// * `time_sec`: T_s
/// * `samp_sec`: F_s
/// * `m`: M
///
/// Where f is frequency:
///
/// f == 1/(m * time_sec)  == samp_sec / m,
/// m = samp_sec / f
///
/// The periodicity of `m` samples in digital domain becomes
/// the periodicity of `1 / m * time_sec` seconds in the
/// physical domain.
///
pub fn sin_periodicity(samp_sec: f64, m: u64) -> f64 {
    1.0 / (m as f64 / samp_sec)
}


/// Moving average
///
/// * `seq`: A digital sequence.
/// * `n`: The index to calcualte the moving average at.
///
pub fn moving_average(seq: Vec<i8>, n: usize) -> f64 {
    if n == 0 {
        seq[n] as f64 / 2.0
    } else {
        (seq[n] + seq[n-1]) as f64 / 2.0
    }
}

#[macro_use] extern crate assert_approx_eq;
#[cfg(test)]
mod tests {
    #[test]
    fn test_sin_periodicity() {
        assert_approx_eq!(436.363636363636, super::sin_periodicity(48000.0, 110));
        assert_approx_eq!(50.0, super::sin_periodicity(44000.0, 880));
    }

    #[test]
    // For the delta signal all values are zero except for the zero index.
    fn test_moving_average_delta() {
        let delta = vec![1, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        assert_approx_eq!(0.5, super::moving_average(delta.clone(), 0));
        assert_approx_eq!(0.5, super::moving_average(delta.clone(), 1));

        // All other values should be zero
        for i in 2..9 {
            assert_approx_eq!(0.0, super::moving_average(delta.clone(), i));
        }
    }

    #[test]
    // For the unit step signal all values are one ecept for those
    // less than zero.
    fn test_moving_average_unit_step() {
        let unit_step = vec![1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
        assert_approx_eq!(0.5, super::moving_average(unit_step.clone(), 0));

        // All other values should average to 1
        for i in 1..9 {
            assert_approx_eq!(1.0, super::moving_average(unit_step.clone(), i));
        }
    }

    /*
    #[test]
    // x[n] = cos(omega * n), omega = pi/10
    fn test_moving_average_sinusoid() {
        let sinusoid = {
            let sin_builder = vec![];
            for n in 0..9 {
                sin_builder.push((f64::consts::PI / 10.0 * n as f64).cos()); 
            }
            sin_builder
        };
        assert_approx_eq!(0.5, super::moving_average(sinusoid.clone(), 0));
    }
    */

    #[test]
    fn test_moving_average_alternating_seq() {
        let alternating = vec![1, -1, 1, -1, 1, -1, 1, -1, 1, -1];
        assert_approx_eq!(0.5, super::moving_average(alternating.clone(), 0));

        for i in 1..9 {
            assert_approx_eq!(0.0, super::moving_average(alternating.clone(), i));
        }
    }
}
