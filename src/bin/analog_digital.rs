//! Analog and Digital
//!
//! Display the signal to noise ratio (SNR) between a digital
//! version of the file and the analog.
extern crate rodio;
extern crate hound;
extern crate num;
extern crate dataplotlib;
extern crate nalgebra;
extern crate tempfile;

use std::env;
use std::f64;
use std::thread;
use std::fs::File;
use std::time::Duration;
use std::io::{BufReader, BufWriter, Write};
use hound::{WavReader, WavWriter, WavSpec, SampleFormat};
use dataplotlib::util::zip2;
use dataplotlib::plotbuilder::PlotBuilder2D;
use dataplotlib::plotter::Plotter;
use rodio::Source;
use nalgebra::DVector;
use tempfile::tempfile;

/// Get frequency from a `.wav` file
fn waveform(filename: &str) -> Vec<f64> {
    let mut reader = WavReader::open(filename).expect("Failed to open WAV file");
    reader.samples::<i16>()
        .map(|x| x.unwrap() as f64)
        .collect::<Vec<_>>()
}

/// Play an audio file from filename
fn play_audio(filename: &str) {
    let endpoint = rodio::get_default_endpoint().unwrap();
    let file = File::open(filename).unwrap();
    let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
    rodio::play_raw(&endpoint, source.convert_samples());
}

/// Signal to Noise Ratio
///
/// Returns the signal to noise ratio in dBs.
///
fn snr(processed: Vec<i64>, original: Vec<f64>) -> f64 {
    let signal_to_noise: Vec<f64> = original.iter().cloned()
        .zip(processed.iter().cloned())
        .map(|(o, p)| o - p as f64)
        .collect();

    // Power of the error
    let err = DVector::from_iterator(signal_to_noise.len(), signal_to_noise.iter().cloned()).norm();

    // Power of the signal
    let sig = DVector::from_iterator(original.len(), original.iter().cloned()).norm();

    10.0 * (sig / err).log10()
}

/// Write out a temp wav file
fn temp_wav(wav: Vec<i64>) -> tempfile::NamedTempFile {
    let file = tempfile::NamedTempFile::new().unwrap();

    let header = WavSpec {
        channels: 1,
        sample_rate: 1600,
        bits_per_sample: 16,
        sample_format: SampleFormat::Int,
    };

    {
        let buf_writer = BufWriter::new(&file);
        let mut writer = WavWriter::new(buf_writer, header).expect("Failed to created WAV writer");
        for sample in wav {
            writer.write_sample(sample as i16).unwrap();
        }
        writer.finalize();
    }

    file
}

fn main() {
    println!("[*] Processing");
    let fname = env::args().nth(1).expect("no file given");

    // Rescale the analog signal between -100 and +100
    // Keeps quality normalizing the scaling.
    let analog: Vec<f64> = {
        let wf = waveform(&fname);

        // NOTE: See http://stackoverflow.com/a/28248065/155423
        // for how to do this correctly.
        let norm = 1.0 / wf.iter().cloned().fold(-1./0., /* -inf */ f64::max);
        wf.iter().map(|x| 100.0 * x * norm).collect()
    };

    // Digital version is limited to integers.
    let digital: Vec<i64> = analog.iter()
        .cloned()
        .map(|x| x.round() as i64)
        .collect();

    let signal_to_noise: Vec<f64> = analog.iter().cloned()
        .zip(digital.iter().cloned())
        .map(|(a, d)| a - d as f64)
        .collect();

    let err = snr(digital.clone(), analog);
    println!("[*] Signal to noise ratio: {} dB", err);
    
    let digitized_file = temp_wav(digital);

    println!("[*] Plotting");
    let mut xs = vec![];
    for (x, _) in signal_to_noise.iter().enumerate() {
        xs.push(x as f64);
    }
    let xys = zip2(&xs, &signal_to_noise);
    let mut pb = PlotBuilder2D::new();
    pb.add_color_xy(xys, [0.0, 0.0, 1.0, 1.0]);

    let mut plt = Plotter::new();
    plt.plot2d(pb);

    thread::sleep(Duration::from_millis(2600));
    play_audio(&fname);

    let digitized_fname = digitized_file.path()
        .to_str().unwrap();
    play_audio(digitized_fname);

    plt.join();
}
