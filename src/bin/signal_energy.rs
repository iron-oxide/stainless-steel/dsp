//! Signal Energy
//! 
//! Signal energy in DSP is understood as a sum of squared
//! norms of all the (discrete) signal's samples. To quote
//! the SP4COMM book:
//!
//! > This definition is consistent with the idea that, if
//! > the values of the sequence represent a time-varying
//! > voltage, the above sum would express the total energy
//! > (in joules) dissipated over a 1Ω-resistor.
extern crate hound;
extern crate num;

use std::env;
use hound::{WavReader, WavSamples};

trait Signal {
        fn energy(self) -> f64;
}

impl<'a, R> Signal for WavSamples<'a, R, i16>
    where R: std::io::Read {

	fn energy(self) -> f64 {
		self.map(|x| {
			let sample = x.unwrap() as f64;
			sample * sample
		})
		.sum()
	}
}

fn main() {
    let fname = env::args().nth(1).expect("no file given");
    let mut reader = WavReader::open(fname).expect("Failed to open WAV file");
    let energy = reader.samples().energy();
    println!("Signal Energy: {}", energy);
}
