//! Waveform
//!
//! Display the waveform of a `.wav` file.
extern crate rodio;
extern crate hound;
extern crate num;
extern crate dataplotlib;

use std::env;
use std::thread;
use std::fs::File;
use std::time::Duration;
use std::io::BufReader;
use hound::WavReader;
use dataplotlib::util::zip2;
use dataplotlib::plotbuilder::PlotBuilder2D;
use dataplotlib::plotter::Plotter;
use rodio::Source;

fn waveform(filename: &str) -> Vec<f64> {
    let mut reader = WavReader::open(filename).expect("Failed to open WAV file");
    reader.samples::<i16>()
        .map(|x| x.unwrap() as f64)
        .collect::<Vec<_>>()
}

fn play_audio(filename: &str) {
    let endpoint = rodio::get_default_endpoint().unwrap();
    let file = File::open(filename).unwrap();
    let source = rodio::Decoder::new(BufReader::new(file)).unwrap();
    rodio::play_raw(&endpoint, source.convert_samples());
}

fn main() {
    println!("[*] Processing");
    let fname = env::args().nth(1).expect("no file given");
    let ys = waveform(&fname);

    println!("[*] Plotting");
    let mut xs = vec![];
    for (x, _) in ys.iter().enumerate() {
        xs.push(x as f64);
    }
    let xys = zip2(&xs, &ys);
    let mut pb = PlotBuilder2D::new();
    pb.add_color_xy(xys, [0.0, 0.0, 1.0, 1.0]);

    let mut plt = Plotter::new();
    plt.plot2d(pb);

    thread::sleep(Duration::from_millis(2600));
    play_audio(&fname);

    plt.join();
}
