//! Sepectral Peak
//!
//! Find the most dominant frequency in the signal.
extern crate hound;
extern crate num;
extern crate rustfft;

use std::env;
use num::complex::Complex;
use rustfft::FFT;
use hound::WavReader;

fn find_spectral_peak(filename: &str) -> Option<f32> {
    let mut reader = WavReader::open(filename).expect("Failed to open WAV file");
    let num_samples = reader.len() as usize;
    let mut fft = FFT::new(num_samples, false);
    let signal = reader.samples::<i16>()
        .map(|x| Complex::new(x.unwrap() as f32, 0f32))
        .collect::<Vec<_>>();
    let mut spectrum = signal.clone();
    fft.process(&signal[..], &mut spectrum[..]);
    let max_peak = spectrum.iter()
        .take(num_samples / 2)
        .enumerate()
        .max_by_key(|&(_, freq)| freq.norm() as u32);
    if let Some((i, _)) = max_peak {
        let bin = 44100f32 / num_samples as f32;
        Some(i as f32 * bin)
    } else {
        None
    }
}

fn main() {
    let fname = env::args().nth(1).expect("no file given");
	if let Some(peak) = find_spectral_peak(&fname) {
		println!("Max frequency: {} Hz", peak);
	} else {
		println!("Unable to determine max frequency");
	}
}
